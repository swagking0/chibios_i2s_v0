 ChibiOs_I2S_v0

 Implement I2S interface

*****************************************************************************
*** Files Organization                                                    ***
*****************************************************************************
--{root}                                        - ChibiOS directory.
    +--os/                                      - ChibiOS components.
        +--include/                             - i2s.h is added and in hal.h i2s.h is defined.
        +--src/                                 - i2s.c is added.
        hal.mk                                  - path for i2s package.
        +--platforms                            - 
            +--STM32                            - 
                +--SPIv1                        - i2s_lld.c and i2s_lld.h is added.
                +--SPIv2                        - i2s_lld.c and i2s_lld.h is added.
            +--STM32F1xx                        - 
                platform_f105_f107.mk           - path for i2s_lld.c and i2s_iid.h package.
                platform.mk                     - path for i2s_lld.c and i2s_iid.h package.
    +--testhal/                                 - HAL integration test demos.
    |  +--STM32F1xx/                            - Created I2S directory inside here.
    |   |  +--I2S/                              - added all file.