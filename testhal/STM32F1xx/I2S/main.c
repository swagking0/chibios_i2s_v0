
#include "ch.h"
#include "hal.h"

#define I2S_BUF_SIZE                         256

static uint16_t i2s_rx_buf[I2S_BUF_SIZE];

static void i2scallback(I2SDriver *i2sp, size_t offset, size_t n);

static const I2SConfig i2scfg = {
  NULL,
  i2s_rx_buf,
  I2S_BUF_SIZE,
  i2scallback,
  0,
  16
};

static void i2scallback(I2SDriver *i2sp, size_t offset, size_t n) {
  (void)i2sp;
  (void)offset;
  (void)n;
}

/*
  * Application enter point
*/

int main(void){
  halInit();
  chSysInit();


  i2sStart(&I2SD2, &i2scfg);
  // palSetPadMode(GPIOB, 10, PAL_MODE_ALTERNATE(5));
  // palSetPadMode(GPIOB, 3, PAL_MODE_ALTERNATE(5));

  i2sStartExchange(&I2SD2);

  while (true) {
    // if(palReadPad(GPIOA, GPIOA_BUTTON))
    //   i2sStopExchange(&I2SD2);
    chThdSleepMilliseconds(30000);
    i2sStopExchange(&I2SD2);
  }
  return 0;
}
